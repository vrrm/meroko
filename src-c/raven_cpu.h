/* Raven (Explorer 1) CPU - Header

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

*/


void raven_initialize();
void raven_clockpulse();
void raven_dispupdate();
void raven_halt();
void raven_step();
void raven_cont();
void raven_dump();

