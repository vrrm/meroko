/* Meroko's Common Definitions - Placeholder

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

 $Id$
*/

#ifndef MEROKO_H
#define MEROKO_H

#include <inttypes.h>

/* We have five traces levels which we fit into a 64 bit integer mask */
/* - error these can't ordinarily be turned off  */
/*   0x00 - 0x0F */
/* - warning anything that might be a problem (that doesn't happen too often) */
/*   0x10 - 0x1F */
/* - Low level hardware event that happens intermitently */
/*   0x20 - 0x2F */
/* - event that happens (most every) CPU cycle */
/*   0x30 - 0x3F */

#define ERROR_TRACE_EVENT 0x010

#ifdef WARN_TRACELOG
#define WARN_TRACE_EVENT 0x020
#else
#define WARN_TRACE_EVENT 0
#endif

#ifdef HARDWARE_TRACELOG
#define HARDWARE_TRACE_EVENT 0x040
#else
#define HARDWARE_TRACE_EVENT 0
#endif

#ifdef CYCLE_TRACELOG
#define CYCLE_TRACE_EVENT 0x080
#else
#define CYCLE_TRACE_EVENT 0
#endif

#ifdef CPU_TRACELOG
#define CPU_TRACE_EVENT 0x100
#else
#define CPU_TRACE_EVENT 0
#endif

#define TRACE_LOG_MASK  (WARN_TRACE_EVENT | HARDWARE_TRACE_EVENT | \
		      CYCLE_TRACE_EVENT | CPU_TRACE_EVENT)

typedef unsigned char uint8;
typedef int int32;
typedef unsigned int uint32;

void logmsg(char *msg);
void logmsgf(char *fmt, ...);
void llogmsgf(int level, char *fmt, ...);

#endif /* MEROKO_H */
