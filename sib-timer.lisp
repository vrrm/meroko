;;;
;;; sib-timer.lisp
;;;
;;; SIB board interval timers
;;;

(in-package :nevermore)

;; This looks like the same 8253/8254 PIT that the PC uses,
;; clocked at once every 7 microcycles (1 MHz).

(defvar *sib-timer-0-count* 0)
(defvar *sib-timer-0-latch* 0)
(defvar *sib-timer-0-byte* 0)

(defun sib-timer-read (address)
  ;(declare (ignorable address))
  (if (= (ldb (byte 2 2) address) 0)
      (progn
	(setf *sib-timer-0-byte* (logxor *sib-timer-0-byte* 8))
	(setf *inhibit-nubus-trace* t)
	(ldb (byte 8 *sib-timer-0-byte*) *sib-timer-0-latch*))
      0))

(defun sib-timer-write (address data)
  (declare (ignorable address data))
  (setf *sib-timer-0-latch* *sib-timer-0-count*)
  (setf *sib-timer-0-byte* 8)
  (setf *inhibit-nubus-trace* t))

(defun sib-timer-clock ()
  (setf *sib-timer-0-count* (logand #xffff (1- *sib-timer-0-count*))))

(declaim (fixnum *sib-timer-7cycle-count*))
(defvar *sib-timer-7cycle-count* 0)

(defun sib-timer-microcycle-handler ()
  (if (zerop *sib-timer-7cycle-count*)
      (progn
	(sib-timer-clock)
	(setf *sib-timer-7cycle-count* 6))
      (decf *sib-timer-7cycle-count*)))

(add-microcycle-hook #'sib-timer-microcycle-handler)

;;; EOF
