
#ifdef DISPLAY_FB
void redraw_display(){
  // Translate to 8bpp and post result
  uint32_t FBaddr=0;
  uint64_t x=1;
  uint32_t row,col;
  uint32_t VRAMaddr=0;

  while(VRAMaddr < 0x20000){
    col = VRAMaddr*8;
    row = (col/1024);
    col -= (row*1024); // Remove excess
    x=1;
    if(col < FB_WIDTH && row < FB_HEIGHT){
      FBaddr = (row*FB_WIDTH)+col;
      // Invert video if required.
      // This is structured like this to cut down on bit tests required in one pass.
      if((sib_video_attr&0x02) == 0x02){
	// Reverse Video
        while(x < 0x100000000LL){
          if((VRAM[VRAMaddr]&x)==x){
            framebuffer[FBaddr]=PIX_Z; // ON
          }else{
            framebuffer[FBaddr]=PIX_NZ; // OFF
          }
          x = x << 1;
          FBaddr++; // Next pixel
        }
      }else{
        // Normal Video
        while(x < 0x100000000LL){
          if((VRAM[VRAMaddr]&x)==x){
            framebuffer[FBaddr]=PIX_NZ; // ON
          }else{
            framebuffer[FBaddr]=PIX_Z; // OFF
          }
          x = x << 1;
          FBaddr++; // Next pixel
        }
      }
      //      FBaddr = row*col;
      FBaddr = (row*FB_WIDTH)+col;
    }
    VRAMaddr++;
  }
#ifdef DISPLAY_FB
  // Redraw the whole screen
  msync(framebuffer,screensize,MS_SYNC);
#endif
#ifdef DISPLAY_SDL
  sdl_sync(framebuffer, screensize);
#endif
}
#endif // DISPLAY_FB
