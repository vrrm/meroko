/* Local Bus header

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

*/

/* local bus Interface */
extern int LCbus_error;
extern int LCbus_acknowledge;
extern uint32_t LCbus_Address;
extern uint32_t LCbus_Request;
extern uint32_t LCbus_Data;

extern uint32_t nubus_io_request(int access, uint32_t address, uint32_t data, int owner);
