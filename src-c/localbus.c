/* Local bus Implementation

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

*/

/* local bus interface */

#include "meroko.h"

int LCbus_error;
int LCbus_acknowledge;
uint32_t LCbus_Address;
uint32_t LCbus_Request;
uint32_t LCbus_Data;
