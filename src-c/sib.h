/* System Interface Board - Header

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

*/

void sib_init();
void sib_nubus_io();

