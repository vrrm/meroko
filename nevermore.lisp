(in-package :nevermore)

(defun configure-nevermore ()
  (setf *rom-file-path* (merge-pathnames #p"dev/meroko/nevermore/proms/" (user-homedir-pathname)))
  (let ((disk-dir (merge-pathnames  #p"dev/meroko/src-c/X1-DISKS/" (user-homedir-pathname))))
    (setf *nupi-scsi0-0-disk-file* (merge-pathnames #p"c0-d0.dsk" disk-dir))
    (setf *nupi-scsi0-1-disk-file* (merge-pathnames #p"c0-d1.dsk" disk-dir))
    (setf *nupi-scsi2-0-disk-file* (merge-pathnames #p"c2-d0.dsk" disk-dir))))


(defun run-nevermore ()
  (configure-nevermore)
  (load-romfiles)
  (setf *rom-file-path* (merge-pathnames #p"dev/meroko/nevermore/proms/" (user-homedir-pathname)))
  (load-romfiles)
  (sib-init-character-recognizer)
  (sib-init-keypress-stuffer)
  (start-x)
  (init-window)
  (raven::microengine-initialize)
  (raven::microengine-run-to -1))


(print "Quickstart: try typing (nevermore::run-nevermore)")
