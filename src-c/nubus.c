/* Nubus Implementation

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

*/

/* NUbus Interface */

#include "meroko.h"

int Nubus_Busy;
int NUbus_error;
int NUbus_acknowledge;
uint32_t NUbus_Address;
uint32_t NUbus_Request;
uint32_t NUbus_Data;
