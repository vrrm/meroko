/* 8MB Memory Board - Header

 This file is subject to the terms and conditions of the GNU General Public
 License.  See the file COPYING in the main directory of this archive for
 more details.

*/


void mem8_init();
void mem8_nubus_io();
void mem8_lcbus_io();

