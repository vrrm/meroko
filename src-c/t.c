/* typedef uint32_t uint32; */
/* typedef int64_t int64; */

uint32_t Mbus; // M-bus
uint32_t Abus; // A-bus
int64_t ALU_Result

main()
{
	Mbus = 0x
  ALU_Result = Mbus - Abus;
  // logmsgf("ALU: Result: 0x%lX - 0x%lX = 0x%LX\n",Mbus,Abus,ALU_Result);
  if(MInst_ALU_Carry_In == 0){
    ALU_Result -= 1;
    // logmsgf("After Carry-In Handling - 0x%LX\n",ALU_Result);
  }
  // FIXNUM Overflow Check
  if((((Mbus^Abus)&(Mbus^ALU_Result))&0x01000000)==0x01000000){
    ALU_Fixnum_Oflow=1;
  }
//brad-I think the following doesn't do anything; the bit will be set
//already if there is carry
  // HACKHACK is this correct?
  if(ALU_Result < Mbus){
    ALU_Result |= 0x100000000LL; // Arrange for carry-out
  }
}

inline void alu_cleanup_result(){
  // Reduce carry-out to a flag without use of boolean type
  if((ALU_Result&0xFFFFFFFF00000000LL) != 0){ ALU_Carry_Out = 1; }
  // Clean Output (ALU is 32 bits wide)
  ALU_Result &= 0xFFFFFFFF;

  // Make the result
  Obus = ALU_Result;
}
